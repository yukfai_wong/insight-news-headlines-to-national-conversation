# Profitable data analysis #

This repository included two data analysis project:

1. Map news headlines to national conversation
2. Regional socio-economic conditions to state economic conditions

The first project has used word count and spacy to extract meaning action and entities to build the image of the conversation over the last decade. It can benefit the reporter, journalist and media as it reflect the focus of people. An insight report is also included for the critical analysis includes Ethical considerations, principles of best practice in data analytics, and potential
consequences.


The second project analysis the relation of housing-market indicators to national social and economic conditions. The project involve ploting and comparing various dataset to find similar tendes. It showen that housing-market indicators and national social economic conditions is inversely proportional. 

